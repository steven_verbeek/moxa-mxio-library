{
    "targets": [
        {
            "target_name": "Moxa",
            "sources": [ "Moxa.cc", "functions.cc" ],
            "include_dirs" : [
				'/usr/local/include',
 	 			"<!(node -e \"require('nan')\")"
			],
			'link_settings': {
				'libraries': [
				  '/usr/local/lib/libmxio_x64.a'
				]
			  },
        }
    ],
}
