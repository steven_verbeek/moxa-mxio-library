# Node MXIO MOXA Library

Example of use

Start the library

    var moxa = require('./build/Release/Moxa.node');

Initialize MOXA or MXIO

    moxa.moxa_init();

Connect to the IP with IP and Port

    var handle = moxa.moxa_connect("192.168.127.254",502);

Set a value to the handle (in this case we are sending 1)

    var send = moxa.moxa_setoutput(handle,1);

Read output from the handle

    var read = moxa.moxa_read(handle, 0, function(value) {
	    console.log(value);
    });
