#include <node.h>
#include <nan.h>
#include "functions.h"

using v8::FunctionTemplate;
using v8::Handle;
using v8::Object;
using v8::String;
using v8::Local;

void InitAll(Handle<Object> exports) {
  exports->Set(NanNew<String>("moxa_init"),
    NanNew<FunctionTemplate>(moxa_init)->GetFunction());
  exports->Set(NanNew<String>("moxa_setoutput"),
    NanNew<FunctionTemplate>(moxa_setoutput)->GetFunction());
  exports->Set(NanNew<String>("moxa_connect"),
    NanNew<FunctionTemplate>(moxa_connect)->GetFunction());
  exports->Set(NanNew<String>("moxa_read"),
    NanNew<FunctionTemplate>(moxa_read)->GetFunction());
}

NODE_MODULE(Moxa, InitAll)
