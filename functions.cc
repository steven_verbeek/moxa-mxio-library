#include <nan.h>
#include "functions.h"
#include "mxio.h"
#include <iostream>

char *get(v8::Local<v8::Value> value, const char *fallback = "") {
    if (value->IsString()) {
        v8::String::Utf8Value string(value);
        char *str = (char *) malloc(string.length() + 1);
        strcpy(str, *string);
        return str;
    }
    char *str = (char *) malloc(strlen(fallback) + 1);
    strcpy(str, fallback);
    return str;
}

NAN_METHOD(moxa_init) {
    NanScope();
    NanReturnValue(MXEIO_Init());
}

NAN_METHOD(moxa_setoutput) {
    NanScope();
    
    int iRet;
    DWORD dwValue[16] = {'\0'};
    BYTE bytCount = 1;
    
    int iHandle = args[0]->Uint32Value();
    int value = args[1]->Uint32Value();
    int bytStartChannel = args[2]->Uint32Value();

	dwValue[0] = value;
    iRet = E1K_DO_Writes( iHandle, bytStartChannel, bytCount, dwValue[0]);
   
    NanReturnValue(iRet);
}

NAN_METHOD(moxa_connect) {
    NanScope();
    
    int iRet;           //stored return code
    char Password[8] = {'\0'};
    int iHandle;
    
    char *ip = get(args[0], "192.168.127.254");
    int port = args[1]->Uint32Value();
   
    iRet = MXEIO_E1K_Connect(ip, port, 2000, &iHandle, Password);
   
    if(iRet == MXIO_OK)
        NanReturnValue(NanNew<v8::Number>(iHandle));
    else
        NanReturnValue(NanNew<v8::Number>(iRet));
}

NAN_METHOD(moxa_read) {
	NanScope();
	
	int iHandle = args[0]->Uint32Value();
	int bytStartChannel = args[1]->Uint32Value();
	v8::Local<v8::Function> callbackHandle = args[2].As<v8::Function>();
	
	int iRet;
	BYTE bytCount = 8;
	DWORD dwValue[16] = {'\0'};
	
	while(1)
	{
		iRet = E1K_DI_Reads(iHandle, bytStartChannel, bytCount, dwValue);
		
		v8::Local<v8::Value> v = NanNew(dwValue[0]);
		NanMakeCallback(NanGetCurrentContext()->Global(), callbackHandle, 1, &v);
	}

	NanReturnValue(NanNew<v8::Number>(iRet));
}
