#ifndef MOXA_GRAB_H
#define MOXA_GRAB_H

#include <nan.h>

NAN_METHOD(moxa_init);
NAN_METHOD(moxa_setoutput);
NAN_METHOD(moxa_connect);
NAN_METHOD(moxa_read);

#endif
